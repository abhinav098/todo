module TodoListsHelper

  def correct_user?(user)
    current_user == user
  end

end
